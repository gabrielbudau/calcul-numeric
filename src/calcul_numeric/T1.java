package calcul_numeric;
import static java.lang.Math.*;
/**
 *
 * @author Gabriel Budau
 */
public class T1 extends Solver{
    
    public void exec(){
        double rez = 0.0;
        //===========================================
        // 1) Singura radacina negativa
        //===========================================
        System.out.println("\n\n1) Singura radacina negativa");
        System.out.println(" x = " + metBisectiei(-1, 0) + " f(x)= " + f(metBisectiei(-1, 0)) + " Metoda Bisectiei");
        System.out.println(" x = " + metBisectieiFalsi(-1, 0) + " f(x)= " + f(metBisectieiFalsi(-1, 0)) + " Metoda Bisectiei Falsi");
        rez = metNewton(-0.5);
        System.out.println(" x = " + rez + " f(x)= " + f(rez) + "Metoda Newton");
        rez = metSecantei(-0.5, -0.49);
        System.out.println(" x = " + rez + " f(x)= " + f(rez) + " Metoda Secantei");
        rez = metCoardei(-1.0 ,0.0);
        System.out.println(" x = " + rez + " f(x)= " + f(rez) + " Metoda Coardei");
        System.out.println("\n");

        //===========================================
        // 2) Cea mai mica radacina pozitiva
        //===========================================
        System.out.println("2) Cea mai mica radacina pozitiva");
        System.out.println(" x = " + metBisectiei(0.00000000001, 1) + " f(x)= " + f(metBisectiei(0.00000000001, 1)) + " Metoda Bisectiei");
        System.out.println(" x = " + metBisectieiFalsi(0.00000000001, 1) + " f(x)= " + f(metBisectieiFalsi(0.00000000001, 1)) + " Metoda Bisectiei Falsi");
        rez = metNewton(0.0);
        System.out.println(" x = " + rez + " f(x)= " + f(rez) + " Metoda Newton");
        rez = metSecantei(0.0, 0.001);
        System.out.println(" x = " + rez + " f(x)= " + f(rez) + " Metoda Secantei");
        rez = metCoardei(0.0 ,1);
        System.out.println(" x = " + rez + " f(x)= " + f(rez) + " Metoda Coardei");
        System.out.println("\n");

        //===========================================
        // 3) Primele 20 radacini pozitive
        //===========================================
        System.out.println("3) Primele 20 radacini pozitive");
        System.out.println("Metoda Bisectiei");
        
        double b = 1, i = 0.0;
        rez = metBisectiei(0.00000000001, b);
        while (i < 20) {
            System.out.println("x = " + rez + " f(x)= " + f(rez));
            rez = metBisectiei(rez + prec, b + 1);
            b++;
            i++;
        }

        System.out.println("\n\nMetoda Bisectiei Falsi");
        rez = metBisectieiFalsi(0.00000000001, 1);
        b = 1;
        i = 0.0;
        rez = metBisectiei(0.00000000001, b);
        while (i < 20) {
            System.out.println("x = " + rez + " f(x)= " + f(rez));
            rez = metBisectiei(rez + prec, b + 1);
            b++;
            i++;
        }
        
        System.out.println("\n\nMetoda Coardei");
        b = 1;
        i = 0.0;
         rez = metCoardei(0.0, 1);
        while (i < 20) {
            System.out.println("x = " + rez + " f(x)= " + f(rez));
            rez = metCoardei(rez + prec, b + 1);
            b++;
            i++;
        }

        System.out.println("\n\nMetoda Newton");
        rez = metNewton(0.0);
        i = 1.0;
        b = 0.0;
        while (i <= 20) {
            System.out.println("x = " + rez + " f(x)= " + f(rez));
            rez = metNewton(i);
            i++;
        }

        System.out.println("\n\nMetoda Secantei");
        rez = metSecantei(0.0, 0.00001);
        //System.out.println("x = " + rez + " ");
        i = 0.0;
        b = 1.0;
        while (i < 20) {
            double temp_rez = rez;
            rez = metSecantei(b, b + 0.00001);
            if (rez > 0 && Double.compare(rez, temp_rez) != 0) {
                System.out.println("x = " + rez + " f(x)= " + f(rez));
                i++;
            }
            b++;

        }
    }
    
    @Override
    double f(double x) {
        return log(1 + (x * x)) - (exp(0.4 * x) * cos(PI * x));
    }
}
