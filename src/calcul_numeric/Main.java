package calcul_numeric;

/**
 *
 * @author Gabriel Budau
 */
public class Main {
    /*
    public static void main(String[] args) {
        new T1().exec();
    }*/
    
     public static void main(String args[])
    {
        System.out.print("Enter the Number of Terms : ");
        SistemDeEcuatii obj=new SistemDeEcuatii(4);
        obj.fillMatrix();
        System.out.println("\fYou Have Entered The Following Equations :");
        obj.printMatrix();
        obj.eliminate();
        obj.solve();
        obj.printSolution();
    }
}
