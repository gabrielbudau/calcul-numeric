/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package calcul_numeric;

import java.util.*;

/**
 *
 * @author Gabriel
 */
public class SistemDeEcuatii {
    
    // INSTANCE VARIABLES
    private double[][] m;    // MATRIX OF CO-EFFICIENTS
    private double[] constants; // VECTOR OF CONSTANT TERMS
    private double[] solution; // SOLUTION SET
    private int numEq;      // NUMBER OF EQUATIONS
    static Scanner sc=new Scanner(System.in);
    
    public SistemDeEcuatii(int equations)   // CONSTRUCTOR
    {
        numEq=equations;
        m=new double[numEq][numEq];
        constants=new double[numEq];
        solution=new double[numEq];
    }
    
    public void fillMatrix()
    {
        
        m[0] = new double[]{1.0, -2.0, 3.0, 4.0};
        m[1] = new double[]{3.0, -1.0, 2.0, 5.0};
        m[2] = new double[]{2.0, 4.0, -5.0, 1.0};
        m[3] = new double[]{4.0, 2.0, -1.0, 3.0};
        constants = new double[]{4.5, 9.5, 15.0, 12.0};
    }
    
    public void printSolution()
    {
        System.out.println("\nSolution Set is : ");
        for(int i=0;i<numEq;i++)
            System.out.println((char)('A'+i)+" = "+solution[i]);
    }
    
    public void printMatrix()   // FOR DEBUGGING PURPOSE
    {
        for(int i=0;i<numEq;i++){
            for(int j=0;j<numEq;j++){
                if(m[i][j]>=0)
                    System.out.print(" +"+m[i][j]+((char)('A'+j))+" ");
                else if(m[i][j]<0)
                    System.out.print(" "+m[i][j]+((char)('A'+j))+" ");
            }
            System.out.println(" = "+constants[i]);
        }
    }
    
    public void swapRows(int row1,int row2)
    {
        double temp;
        for(int j=0;j<numEq;j++){   // SWAPPING CO-EFFICIENT ROWS
            temp=m[row1][j];
            m[row1][j]=m[row2][j];
            m[row2][j]=temp;
        }
        temp=constants[row1];   // SWAPPING CONSTANTS VECTOR
        constants[row1]=constants[row2];
        constants[row2]=temp;
    }
    
    public void eliminate()
    {
        int i,j,k,l;
        for(i=0;i<numEq;i++){   // i -> ROW ; MATRIX ORDER DECREASES DURING ELIMINATION
            // FIND LARGEST CO-EFFICIENTSOF THE CURRENT COLUMN MOVING ROW-WISE
            double largest=Math.abs(m[i][i]);
            int index=i;
            for(j=i+1;j<numEq;j++){
                if(Math.abs(m[j][i])>largest){
                    largest=m[j][i];
                    index=j;
                }
            }
            swapRows(i,index);  // SWAPPING i-th ROW to index-th ROW
            for(k=i+1;k<numEq;k++){
                double factor=m[k][i]/m[i][i];
                // PROCESSING COLUMN WISE
                for(l=i;l<numEq;l++){
                    m[k][l]-=factor*m[i][l];
                }
                constants[k]-=factor*constants[i];  // PROCESSING CONSTANTS
            }
        }
    }
    
    public void solve()
    {
        for(int i=numEq-1;i>=0;i--){
            solution[i]=constants[i];   // COPY
            for(int j=numEq-1;j>i;j--){
                solution[i]-=m[i][j]*solution[j];
            }
            solution[i]/=m[i][i];
        }
    }
    
    public static void main(String args[])
    {
        System.out.print("Enter the Number of Terms : ");
        SistemDeEcuatii obj=new SistemDeEcuatii(sc.nextInt());
        obj.fillMatrix();
        System.out.println("\fYou Have Entered The Following Equations :");
        obj.printMatrix();
        obj.eliminate();
        obj.solve();
        obj.printSolution();
    }
    //http://0code.blogspot.ro/2012/05/gauss-matrix-elimination-java-bluej.html
}
