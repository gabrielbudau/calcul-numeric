package calcul_numeric;

import static java.lang.Math.*;

/**
 *
 * @author Gabriel Budau
 */
public abstract class Solver {

    double prec = 0.0000000001;

    
    public double metCoardei(double a, double b){
        double x0= a;
        double c = b;
        double x1;
        
        if((f(x0) - f(c)) == 0.0)
            return x0;
        x1 = (c*f(x0) - x0*f(c))/(f(x0) - f(c));
        for (int i = 0; i < 1000; i++) {
            x0 = x1;
            if((f(x0) - f(c)) == 0.0)
                return x0;
            x1 = (c*f(x1) - x1*f(c))/(f(x1) - f(c));
        }
        return x1;
    }
    
    
    /**
     *
     * @param a capat stang al intervalului
     * @param b capat drept al intervalului
     * @return un numar ce converge la o radacina a lui f(x) si este in
     * intervalul [a, b]
     */
    public double metBisectiei(double a, double b) {
        double c = 0.0;
        if ((f(a) * f(b)) < 0.0) {
            c = (a + b) / 2;
            while (abs(b - a) >= prec) {
                c = (a + b) / 2;
                if ((f(a) * f(c)) < 0.0) {
                    b = c;
                } else {
                    a = c;
                }
            }
        }
        return c;
    }

    /**
     *
     * @param x0 punctul de start
     * @return un numar ce converge la o radacina a lui f(x)
     */
    public double metNewton(double x0) {
        double xn1 = x0;
        for (double i = 0; i < 1000; i++) {
            xn1 -= f(xn1) / fd(xn1);
        }
        return xn1;
    }

    /**
     *
     * @param x0 primul punct de start
     * @param x1 a 2- a valoare dupa x0 ce poate fi radacina
     * @return un numar ce converge la o radacina a lui f(x)
     */
    public double metSecantei(double x0, double x1) {
        double x2 = 0.0;
        for (double i = 0; i < 1000; i++) {
            if ((f(x1) - f(x0)) != 0.0) {
                x2 = x1 - f(x1) * (x1 - x0) / (f(x1) - f(x0));
                x0 = x1;
                x1 = x2;
            } else {
                return x2;
            }
        }
        return x2;
    }

    /**
     *
     * @param a capat stang al intervalului
     * @param b capat drept al intervalului
     * @return un numar ce converge la o radacina a lui f(x) si este in
     * intervalul [a, b]
     */
    public double metBisectieiFalsi(double a, double b) {
        
        /**
         * Pentru metBisectieiFalsi(0, b) bucla infinita, de ce?
         */
        double c = 0.0;
        if ((f(a) * f(b)) < 0.0) {
            c = ((-1) * b * f(a) + a * f(b)) / (f(b) - f(a));
            while (abs(b - a) >= prec) {
                if (abs(f(b) - f(a)) == 0.0) {
                    return c;
                }
                c = ((-1) * b * f(a) + a * f(b)) / (f(b) - f(a));
                if ((f(a) * f(c)) < 0.0) {
                    b = c;
                } else {
                    a = c;
                }
            }
        }
        return c;
    }

    /**
     * Calculeaza derivata unei ecuatii pentru un x dat
     *
     * @param x valoarea pentru care este calculata derivata ecuatiei
     * @return derivata lui f(x) in punctul x
     */
    public double fd(double x) {
        double h = x * 1e-8;
        if (h == 0) {
            return -1;
        } else {
            return (f(x + h) - f(x - h)) / (2 * h);
        }
    }

    /**
     * Defineste ecuatia pentru un x dat. Metoda este inplementata pentru
     * fiecare caz de a ceea este trecuta ca abstracta
     *
     * @param x valoarea pentru care este calculata ecuatia
     * @return rezultatul lui f(x)
     */
    abstract double f(double x);

}
